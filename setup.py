#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

requirements = [
    # TODO: put package requirements here
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name="efitai_database",
    version="0.1.0",
    description="Utilities, in python, for the creation, validation, "
    + "manipulation, extraction, and analysis of the "
    + "EFIT-AI database.",
    long_description=readme + "\n\n",
    author="EFIT-AI Team",
    author_email="scott.e.kruger@gmail.com",
    url="https://gitlab.com/efit-ai/efitai_database",
    packages=[
        "efitai_database",
    ],
    package_dir={"efitai_database": "efitai_database"},
    include_package_data=True,
    install_requires=requirements,
    license='BSD license',
    zip_safe=False,
    keywords="efitai_database",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Users",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    test_suite="tests",
    tests_require=test_requirements,
)
