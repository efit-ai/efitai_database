EFITAI_DATABASE
===========================================================================

Utilities, in python, for the creation, validation, manipulation, extraction,
and analysis of the EFIT-AI database.
