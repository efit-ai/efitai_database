# from omfit_classes.omfit_eqdsk import OMFITgeqdsk, OMFITkeqdsk, OMFITsrc
from omfit_classes.omfit_namelist import OMFITnamelist
from omfit_classes.namelist import NamelistName
import h5py
import numpy as np

ods = h5py.File("/fusion/projects/theory/mcclenaghanj/magnetic_db_2019/180700.h5", "r")
itimes = ods["equilibrium"]["code"]["parameters"]["time_slice"]

for iitime, itime in enumerate(itimes):
    time = str(int(1000 * np.array(ods["equilibrium"]["time"][iitime]))).zfill(5)
    shot = str(np.array(ods["dataset_description"]["data_entry"]["pulse"])).zfill(6)

    print(shot, time)
    kfile = OMFITnamelist(f"k{shot}.{time}")
    nmls = itimes[itime].keys()
    for nml in nmls:
        kfile[nml] = NamelistName()
        for item in itimes[itime][nml]:
            try:
                tmp = len(itimes[itime][nml][item])
                kfile[nml][item] = np.array(itimes[itime][nml][item])
            except Exception:
                if isinstance(itimes[itime][nml][item][()], bytes):
                    kfile[nml][item] = itimes[itime][nml][item][()].decode()
                else:
                    kfile[nml][item] = itimes[itime][nml][item][()]

    kfile.save()
