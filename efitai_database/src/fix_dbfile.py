#!/usr/bin/env python
"""
File to validate h5 files to see if the conform to various EFIT-AI standards
"""
import argparse
import h5py
import os
import shutil
import validate_fields as vf
import validate_time as vtime


class FixUp:
    def __init__(self, options):
        """ Parse the options and set data members"""
        if options.verbose:
            print("Initializing")

        self.files = options.files
        self.verbose = options.verbose
        self.save_files = options.save_files

        self.fix_items = {}
        if options.add_missing:
            if options.verbose:
                print('Searching for items to fix')
            for items in options.add_missing.split():
                key, val = items.split(",")
                self.fix_items[key] = val

        self.remove_items = {}
        if options.remove:
            if options.verbose:
                print('Searching for items to remove')
            self.remove_items = options.remove.split()

        self.timeslice_fix = options.timeslice_fix

    def fix(self):
        """
        Decide what type of fix up is needed and invoke appropriate method
        """

        if self.fix_items:
            if self.verbose:
                print("Fixing items")
            self.simple_fix()

        if self.remove_items:
            if self.verbose:
                print("Removing items")
            self.remove_fix()

        if self.timeslice_fix:
            if self.verbose:
                print("Fixing time slices")
            self.fix_timeslices()

    def simple_fix(self):
        """
        Add or overwrite any missing data given in options.

        To make it user friendly, we allow a shortname but then this
        adds an additional loop over the validate_fields fields to
        to find the full path (could create dictionary for fast lookup,
        but not worried about performance.)
        """
        for file in self.files:
            if not os.path.exists(file):
                print("File does not exist", file)
                continue
            h5io = h5py.File(file, mode="r+")
            self._vprint(0, file)

            # Loop over things to fit
            for fitem in self.fix_items:
                self._vprint(1, fitem)
                # Loop over set of full names to find the correct path
                for member in vf.validate_list('allsets'):
                    if fitem in member:
                        if not member.endswith(fitem):
                            continue
                        self._vprint(2, member)
                        val = self.fix_items[fitem]
                        print("Adding missing fields: ", member,
                              "with", val)
                        h5io[member] = val
            h5io.close()

        return

    def remove_fix(self):
        """
        Remove any items specified
        """
        for file in self.files:
            if not os.path.exists(file):
                print("File does not exist", file)
                continue
            h5io = h5py.File(file, mode="r+")
            self._vprint(0, file)

            # Loop over things to fit
            for fitem in self.remove_items:
                self._vprint(1, fitem)
                # Loop over set of full names to find the correct path
                for member in vf.validate_list('allsets'):
                    if fitem in member:
                        if not member.endswith(fitem):
                            continue
                        self._vprint(2, member)
                        if member in h5io:
                            del h5io[member]
            h5io.close()

        return

    def fix_timeslices(self):
        """
        If deleting time slices, go ahead and save them
        """
        for file in self.files:
            if not os.path.exists(file):
                print("File does not exist", file)
                continue
            if self.verbose:
                print("Processing ", file)

            h5io = h5py.File(file, mode="r+")
            valid_slices, n_slice_orig = vtime.get_valid_timeslices(h5io)

            eqgrp = h5io.get("equilibrium")
            timegrp = eqgrp.get("time_slice")
            tgrpparams = eqgrp.get("code/parameters/time_slice")

            if len(valid_slices) != n_slice_orig:  # if some slices are bad
                if self.save_files:
                    if not os.path.exists("Save"):
                        os.mkdir("Save")
                    shutil.copy(file, "Save")
                # Handle the time array
                oldtimes = eqgrp.get("time")[()]
                newtimes = oldtimes[valid_slices]
                del eqgrp["time"]
                eqgrp.create_dataset("time", data=newtimes)

                # Now handle the time slices
                self.slice_del_and_mv(timegrp, valid_slices, n_slice_orig)
                self.slice_del_and_mv(tgrpparams, valid_slices, n_slice_orig)
            h5io.close()

    def slice_del_and_mv(self, tgrp, valid_slices, original_len):
        """
        For a list of good time slice, delete them and move the rest
        Example:
             All:  0 1 2 3 4 5 6 7
             Bad:  1 4
             Del:  0   2 3   5 6 7
             New:  0 1 2 3 4 5
        """
        goodset = set(valid_slices)
        empty_spaces = []

        # reindex
        for ind in range(original_len):
            if ind in goodset:
                if empty_spaces:
                    new_ind = empty_spaces.pop(0)
                    dest1 = str(new_ind)  # this is furthest left position
                    dest2 = str(ind)  # move ind to new_ind (shift data left)
                    tgrp.copy(dest2, dest1)
                    timestr = dest2  # str(ind)
                    del tgrp[timestr]  # old_group#delete copied data
                    empty_spaces.append(ind)  # mark space as empty
            else:
                timestr = str(ind)
                del tgrp[timestr]  # delete copied data
                empty_spaces.append(ind)  # mark position as open

    def _vprint(self, indentnum, vstring):
        if self.verbose:
            print(indentnum*"  ", vstring)
        return


def parse_fixargs():
    """
    Routine for parsing arguments
    """
    desc = "Program for fixing EFIT-AI Database files"
    parser = argparse.ArgumentParser(description=desc)

    # All of these should mostly be the same validator.py to simplify usage

    parser.add_argument('files', type=str, nargs='+',
                        help='EFIT-AI Database files')

    rh = "Save (via copy) invalid files to subdirectory (Level 1 only)"
    parser.add_argument("-S", "--save_files", action="store_true", help=rh)

    parser.add_argument(
        "-v", "--verbose", help="Verbose output", action="store_true"
    )

    hp = 'Things to add (Level 0/2 fixes): e.g., "machine,DIII-D pulse,177452"'
    parser.add_argument("-a", "--add_missing", help=hp, default=None)

    hp = 'Space-delimited list of fields to remove: e.g., "machine pulse"'
    parser.add_argument("-r", "--remove", help=hp, default=None)

    hp = "Remove time slices that don't pass equilibrium checks (Level 1 fix)."
    parser.add_argument("-t", "--timeslice_fix", help=hp, action="store_true")

    return parser


def main():
    """
    Parse arguments and options and act accordingly
    """
    parser = parse_fixargs()
    args = parser.parse_args()

    fx = FixUp(args)
    fx.fix()


if __name__ == "__main__":
    main()
