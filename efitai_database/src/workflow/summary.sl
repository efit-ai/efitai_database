#!/bin/sh
#SBATCH -A m3965
#SBATCH --mail-type=ALL
#SBATCH -c 1
#SBATCH -q shared
#SBATCH -t 00:30:00
#SBATCH -C cpu
#SBATCH -o summary-%j.out
#SBATCH -L cfs

# time full process
starttime=$(date +%s)

# execute scripts
cd $SLURM_SUBMIT_DIR
./make_summary.sh

# output time taken
endtime=$(date +%s)
dt=$(( $endtime - $starttime ))
dh=$(( $dt/3600 ))
dt=$(( $dt-3600*$dh ))
dm=$(( $dt/60 ))
ds=$(( $dt-60*$dm ))
printf "Total runtime: %02d:%02d:%02d\n"  $dh $dm $ds
