#!/bin/sh
#SBATCH -A m3965
#SBATCH --mail-type=ALL
#SBATCH -N 2 -c 128
#SBATCH -q debug
#SBATCH -t 00:30:00
#SBATCH -C cpu
#SBATCH -o farmer-%j.out
#SBATCH -L cfs

# required to use taskfarmer
export THREADS=128

# time full process
starttime=$(date +%s)

# launch taskfarmer processes
cd $SLURM_SUBMIT_DIR
runcommands.sh tasks.txt

# submit follow-up job to create summary
sbatch summary.sl

# output time taken
endtime=$(date +%s)
dt=$(( $endtime - $starttime ))
dh=$(( $dt/3600 ))
dt=$(( $dt-3600*$dh ))
dm=$(( $dt/60 ))
ds=$(( $dt-60*$dm ))
printf "Total runtime: %02d:%02d:%02d\n"  $dh $dm $ds
