#!/usr/bin/env bash

# setup required environment
export OMP_NUM_THREADS=1
export HDF5_USE_FILE_LOCKING=FALSE
export ATOM_ROOT=/global/cfs/cdirs/atom/atom_repo
module use /global/cfs/cdirs/atom/atom_repo/modules/PERLMUTTER
module load omfit

# move to directory where OMAS files are
cd $1

# clean-up any unwanted, failed, or conflicting files
[ -f extract_summary.py ] && rm extract_summary.py
[ -f summary.csv ] && rm summary.csv

# copy extract_summary file here (easier than running it out of place...)
cp $SLURM_SUBMIT_DIR/../extract_summary.py .

# execute script
python3 extract_summary.py

# clean up
rm extract_summary.py
