import os
import sys
import io
from contextlib import redirect_stdout

curdir = os.path.dirname(os.path.abspath(__file__))
pardir = os.path.abspath(os.path.join(curdir, ".."))
sys.path.append(pardir)

try:
    import validator
except ModuleNotFoundError:
    print("Can not import 'validator'. Path is:", end="\n")
    print(f"{sys.path}")
    raise


def get_accepted_results():
    """Return list of what the print statements are
       There might be a better way of doing this
    """
    ar = '\nLevel 0: File valid (for set base): 179565.h5\n'
    ar += 'Level 1: 179565.h5, # of invalid times 4\n'
    ar += "Failures found in these files: ['179565.h5']\n"

    return ar


def test_validator():
    """
    Fixtures are callables decorated with @fixture
    """
    curdir = os.path.dirname(os.path.abspath(__file__))
    data_dir = os.path.abspath(os.path.join(curdir,
                                            "..", "..", "data", "diii-d"))
    file_name = os.path.join(data_dir, "179565.h5")

    if not os.path.exists(file_name):
        raise Exception(f'File: {file_name} does not exist. Can not proceed.')
        return

    # Test the argument parsing
    parser = validator.parse_valargs()
    args = parser.parse_args([file_name])
    assert args.verbose is False

    # Test the method by redirecting print statements to a StringIo object
    #  that we can compare with
    args.validate_levels = '0,1'
    val = validator.Validate(args)
    with redirect_stdout(io.StringIO()) as f:
        val.validate_files()
    function_out = f.getvalue()
    accepted_results = get_accepted_results()
    assert function_out == accepted_results
