import os
import sys

curdir = os.path.dirname(os.path.abspath(__file__))
pardir = os.path.abspath(os.path.join(curdir, ".."))
sys.path.append(pardir)

try:
    import extract_equilibrium
except ModuleNotFoundError:
    print("Can not import 'extract_equilibrium'. Path is:", end="\n")
    print(f"{sys.path}")
    raise


def test_extract_equilibria():
    """
    Fixtures are callables decorated with @fixture
    """
    curdir = os.path.dirname(os.path.abspath(__file__))
    data_dir = os.path.abspath(os.path.join(curdir, "..", "..", "data", "diii-d"))
    file_name = os.path.join(data_dir, "179565.h5")

    if not os.path.exists(file_name):
        raise Exception(f'File: {file_name} does not exist. Can not proceed.')
        return

    print('Test the parse_eqargs method')
    parser = extract_equilibrium.parse_eqargs()
    args = parser.parse_args(file_name)
    assert args.verbose is False

    # File has two time slices - only load one
    args.time = "0.312"

    # Test the init
    eqs = extract_equilibrium.Equilibria(args)
    assert eqs.root_name == "equilibria"

    # Test the data read
    eqs.get_from_file(file_name)
    assert "0.312" in eqs.equilibria["DIII-D_179565"]  # Check time

    # Test the dump method
    eqs.dump("dump_179565.h5")
    assert os.path.exists("dump_179565.h5")

    # Test the restore  method
    eqrestore = extract_equilibrium.Equilibria(args)
    eqrestore.restore("dump_179565.h5")
    assert "0.312" in eqrestore.equilibria["DIII-D_179565"]  # Check time
