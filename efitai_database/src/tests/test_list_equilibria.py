import os
import sys
import io
from contextlib import redirect_stdout

curdir = os.path.dirname(os.path.abspath(__file__))
pardir = os.path.abspath(os.path.join(curdir, ".."))
sys.path.append(pardir)

try:
    import list_equilibria
except ModuleNotFoundError:
    print("Can not import 'list_equilibria'. Path is:", end="\n")
    print(f"{sys.path}")
    raise


def get_accepted_results():
    """Return list of what the print statements are
       There might be a better way of doing this
    """
    ar = '179565.h5\n'
    ar += '\t 0 \t 0.112 s\n'
    ar += '\t 1 \t 0.132 s\n'
    ar += '\t 2 \t 0.152 s\n'
    ar += '\t 3 \t 0.172 s\n'
    ar += '\t 4 \t 0.192 s\n'
    ar += '\t 5 \t 0.212 s\n'
    ar += '\t 6 \t 0.232 s\n'
    ar += '\t 7 \t 0.252 s\n'
    ar += '\t 8 \t 0.272 s\n'
    ar += '\t 9 \t 0.292 s\n'
    ar += '\t 10 \t 0.312 s\n'
    ar += '\t 11 \t 0.352 s\n'
    ar += '\t 12 \t 0.372 s\n'
    ar += 'Total number of equilibria: 13\n'
    return ar


def test_list_equilibria():
    """
    Fixtures are callables decorated with @fixture
    """
    curdir = os.path.dirname(os.path.abspath(__file__))
    data_dir = os.path.abspath(os.path.join(curdir,
                                            "..", "..", "data", "diii-d"))
    file_name = os.path.join(data_dir, "179565.h5")

    if not os.path.exists(file_name):
        raise Exception(f'File: {file_name} does not exist. Can not proceed.')
        return

    print('Test the parse_listargs method')
    parser = list_equilibria.parse_listargs()
    args = parser.parse_args([file_name])
    assert args.verbose is False

    # Test the method by redirecting print statements to a StringIo object
    #  that we can compare with
    with redirect_stdout(io.StringIO()) as f:
        list_equilibria.list_equilibria(args)
    function_out = f.getvalue()
    accepted_results = get_accepted_results()
    assert function_out == accepted_results

