#!/usr/bin/env python
"""
Module for converting omas format files to efitaidb files
"""
import os
import datetime
import argparse
import h5py
import numpy as np
import pandas as pd
from extract_equilibrium import Equilibria, parse_eqargs
import matplotlib.pyplot as plt


class ThomsonChannels(Equilibria):
    def __init__(self, options=None):
        """
        Simple constructor for saving info.  Need to call methods to fill
        datastructures
        """
        super().__init__(options=options)

        # Default skips some equilibrium quantities to minimize data
        if options.keep_equilibrium is not True:
            self.excludes = ["profiles_1d", "constraints"]

        # For convenience, make this the same as the key name in the
        # @property function below
        self.root_name = "thomson"
        self._data[self.root_name] = {}
        return

    # Useful extract function/decorator:
    # Allows the referral to self.equilibria as a member
    @property
    def thomson(self):
        return self._data["thomson"]

    def get_from_file(self, input_file, h5in=None, fit=None):
        """
        Simple wrapper for other methods
        """
        # Get handles
        if not h5in:
            h5in = h5py.File(input_file, mode="r")
        self.get_eq_from_file(h5in)
        self.get_thomson_from_file(h5in,fit=fit)
        return

    def get_thomson_from_file(self, hin, fit=None):
        """
        Get thomson data from file.  If it does not exist, return silently
        because could just be getting the equilibria from this particular file
        """
        if not hin:
            print('get_thomson_from_file called with invalid hin')

        if "thomson_scattering" not in hin:
            return

        shotlbl = self._get_shotlabel_from_h5(hin)
        if shotlbl in self.thomson:
            print(f"{shotlbl} already loaded into data.")
            return

        thomson = hin.get("/thomson_scattering/channel")
        htime = hin.get("/thomson_scattering/ids_properties/homogeneous_time")

        # To fix:  Not sure what "homogeneous_time" refers to
        # htime = pd.to_datetime(str(ht[()]).strip("b").strip("'"))

        # Need some reference time.  Since I don't know what homoegeneous time
        # is, just use today's date
        htime = pd.to_datetime(datetime.date.today())
        rlist = []
        zlist = []
        ne = {}
        neerr = {}
        te = {}
        teerr = {}

        # Loop through channels numerically
        clist = [int(c) for c in thomson.keys()]
        clist.sort()
        channels = np.array(clist, dtype=str)

        for channel in channels:
            chgrp = thomson.get(channel)
            posgrp = chgrp.get("position")
            rlist.append(posgrp.get("r")[()])
            zlist.append(posgrp.get("z")[()])
            negrp = chgrp.get("n_e")
            tegrp = chgrp.get("t_e")
            time = negrp.get("time")[()]

            # This is key to getting the "between_time" work in time_ave
            time_dt = htime + pd.to_timedelta(time, unit="s")

            ne[channel] = pd.Series(negrp.get("data")[()], index=time_dt, name=channel)
            neerr[channel] = pd.Series(
                negrp.get("error")[()], index=time_dt, name=channel
            )
            te[channel] = pd.Series(tegrp.get("data")[()], index=time_dt, name=channel)
            teerr[channel] = pd.Series(
                tegrp.get("error")[()], index=time_dt, name=channel
            )

        # Now fill the thomson datastructure
        self.thomson[shotlbl] = {}
        self.thomson[shotlbl]["n_e"] = pd.DataFrame(ne)
        self.thomson[shotlbl]["t_e"] = pd.DataFrame(te)
        self.thomson[shotlbl]["n_e_err"] = pd.DataFrame(neerr)
        self.thomson[shotlbl]["t_e_err"] = pd.DataFrame(teerr)
        self.thomson[shotlbl]["r"] = np.array(rlist)
        self.thomson[shotlbl]["z"] = np.array(zlist)
        self.thomson[shotlbl]["channels"] = channels
        self.thomson[shotlbl]["time_ref"] = htime

        # look for fit data and time averaged profile data already in file
        if not "thomson_scattering" in hin:
            raise Exception("No thomson scattering data in database file.")
        tsgrp = hin["/thomson_scattering"]
        if "profiles" in tsgrp:
            profgrp = tsgrp["profiles"]
            profiles = {}
            profiles["n_e_psi"] = {}
            profiles["t_e_psi"] = {}
            profiles["n_e_err_psi"] = {}
            profiles["t_e_err_psi"] = {}
            profiles["psi"] = {}
            profiles["n_e_fit"] = {}
            profiles["t_e_fit"] = {}
            profiles["n_e_err_fit"] = {}
            profiles["t_e_err_fit"] = {}
            profiles["psi_fit"] = {}
            profiles["neped_fit"] = {}
            profiles["newid_fit"] = {}
            profiles["teped_fit"] = {}
            profiles["tewid_fit"] = {}
            for t in self.get_times(shotlbl):
                grpnm = "t"+str(t)
                if profgrp[grpnm]:
                    try:
                        profiles["n_e_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_fit_"+fit]).flatten()
                        profiles["n_e_err_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_fit_variance_"+fit]).flatten()
                        profiles["t_e_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e_fit_"+fit]).flatten()
                        profiles["t_e_err_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e_fit_variance_"+fit]).flatten()
                        profiles["psi_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_fit_psi_"+fit]).flatten()
                        profiles["neped_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_fit_ped_"+fit]).flatten()
                        profiles["newid_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_fit_wid_"+fit]).flatten()
                        profiles["teped_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e_fit_ped_"+fit]).flatten()
                        profiles["tewid_fit"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e_fit_wid_"+fit]).flatten()
                        self.havefits = True
                    except:
                        self.havefits = False
                        print("DOES NOT HAVE FITS")

                    try:
                        profiles["n_e_psi"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e"]).flatten()
                        profiles["t_e_psi"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e"]).flatten()
                        profiles["n_e_err_psi"][grpnm[1:]] = np.array(profgrp[grpnm+"/n_e_error"]).flatten()
                        profiles["t_e_err_psi"][grpnm[1:]] = np.array(profgrp[grpnm+"/t_e_error"]).flatten()
                        profiles["psi"][grpnm[1:]] = np.array(profgrp[grpnm+"/psi"]).flatten()
                        self.haveprofiles = True
                    except:
                        pass
                        self.haveprofiles = False
            
            if (self.haveprofiles or self.havefits):
                self.thomson[shotlbl]["profiles"] = profiles
        
        hin.close()

        return

    def write_to_file(self, shotlabel, input_file, h5in=None):
        """
        Write time averaged thomson data to file.
        """        
        # Get handles
        if not h5in:
            h5in = h5py.File(input_file, mode="a")
        
        tsgrp = h5in["thomson_scattering"]
        if "profiles" in tsgrp:
            del tsgrp["profiles"]
        profgrp = tsgrp.create_group("profiles")
        profgrp["averaging_window"] = self.window
        for t in self.get_times(shotlabel):
            timegrp = profgrp.create_group("t"+str(t))
            timegrp["n_e"] = self.thomson[shotlabel]["profiles"]["n_e_psi"][t]
            timegrp["t_e"] = self.thomson[shotlabel]["profiles"]["t_e_psi"][t]
            timegrp["n_e_error"] = self.thomson[shotlabel]["profiles"]["n_e_err_psi"][t]
            timegrp["t_e_error"] = self.thomson[shotlabel]["profiles"]["t_e_err_psi"][t]
            timegrp["psi"] = self.thomson[shotlabel]["profiles"]["psi"][t]
        h5in.close()

    def time_ave(self, shotlbl, time=None, window=1000.):
        """
        Average density and temperature at each channel and at the time in the
        time array over the window specified.

        time array and window are assumed to be given in msec.

        If time array is not given, then it gets the data from all equilibria
        available
        """
        # between_time is difficult to work with but handles the Nan's well.
        # Adding time_ref converts from timedelta to datetime
        # and then .time() converts to the datetime.time format that
        # between_time needs.

        # Initializations
        if not time:
            time = self._get_time(shotlbl)
        self.window = window # save in class data
        ntime = len(time)
        tdt = np.zeros(ntime, dtype=datetime.datetime)
        time_ref = self.thomson[shotlbl]["time_ref"]
        nchannels = len(self.thomson[shotlbl]["channels"])
        qave = {}
        for quant in "n_e n_e_err t_e t_e_err".split():
            qave[quant] = {}
            for channel in range(nchannels):
                qave[quant][str(channel)] = np.zeros(ntime)

        # Create average around each time chosen
        i = 0
        for t in time:
            strt = time_ref + pd.to_timedelta(t*1000. - window, unit="ms")
            stop = time_ref + pd.to_timedelta(t*1000. + window, unit="ms")
            for quant in "n_e n_e_err t_e t_e_err".split():
                for channel in range(nchannels):
                    qave[quant][str(channel)][i] = (
                        self.thomson[shotlbl][quant]
                        .between_time(strt.time(), stop.time())
                        .mean()[channel]
                    )
            # tdt[i]=time_ref+pd.to_timedelta(t, unit='ms')
            tdt[i] = pd.to_timedelta(t, unit="ms")
            i += 1
        timedict = {}
        for A, B in zip(time, tdt):
            timedict[str(A)] = B
        self.thomson[shotlbl]["time_indices"] = timedict

        # Now make into a series
        qaves = {}
        for quant in "n_e n_e_err t_e t_e_err".split():
            qaves[quant] = {}
            for channel in range(nchannels):
                qaves[quant][str(channel)] = pd.Series(
                    qave[quant][str(channel)], index=tdt
                )

        # Now make into a dataframe
        for quant in "n_e n_e_err t_e t_e_err".split():
            qavenm = quant + "_ave"
            self.thomson[shotlbl][qavenm] = pd.DataFrame(qaves[quant])

        #print(self.thomson[shotlbl]['t_e_ave'])
        return

    def get_ts_as_psi(self, shotlbl):
        """
        Use the psi in the equilibria datastructure to do the mapping
        for the time-averaged thomson data:  n_e_ave, t_e_ave, ...
        """
        # Do mapping only on average data
        if "n_e_ave" not in self.thomson[shotlbl]:
            print("ERROR:  Must call time_ave first.")
            return

        nchannels = len(self.thomson[shotlbl]["channels"])

        # The time averaging gives signals at each channels.  This is inverting
        # the mapping to give flux function at each time.
        # Start by doing the channel to psi mapping
        profiles = {}
        profiles["n_e_psi"] = {}
        profiles["t_e_psi"] = {}
        profiles["n_e_err_psi"] = {}
        profiles["t_e_err_psi"] = {}
        profiles["psi"] = {}
        for timenm in self.equilibria[shotlbl]:
            time = float(timenm)  # Need to convert to float
            psip = np.zeros(nchannels, dtype=np.double)
            eq = self.equilibria[shotlbl][timenm]
            kk = list(eq.keys())[0]
            eq = eq[kk]
            for i in range(nchannels):
                rp = self.thomson[shotlbl]["r"][i]
                zp = self.thomson[shotlbl]["z"][i]
                psip[i],_ = self.getPsi(eq, rp, zp)

            psi_map = np.argsort(psip)  # Need to check
            profiles["psi"][timenm] = psip[psi_map]

            for quant in "n_e n_e_err t_e t_e_err".split():
                qavenm = quant + "_ave"
                timeind = self.thomson[shotlbl]["time_indices"][timenm]
                qave = self.thomson[shotlbl][qavenm]

                # get the value at the right time for all channels
                nchannels = len(self.thomson[shotlbl]["channels"])
                qpsi = np.zeros(nchannels)
                for i in range(nchannels):
                    qpsi[i] = qave[str(i)][timeind]

                # store in profiles dictionary of dictionaries
                profiles[quant+"_psi"][timenm] = qpsi[psi_map]

        self.thomson[shotlbl]["profiles"] = profiles
        self.haveprofiles = True

        return

    def plot_eq_channel(self, shotlabel, ax=None, time=None):
        """
        Plot the channel positions on top of the equilibrim
        """
        # setup figure
        import plot_equilibrium
        showplot = False
        if (ax == None):
            fig = plt.figure(figsize=(6,10))
            ax = fig.add_subplot(111)
            showplot = True

        if (time==None):
            tt = list(self.get_times(shotlabel))
            time = tt[len(tt)//2]

        # plot equilibrium
        eq = self.equilibria[shotlabel]
        eqt = self.equilibria[shotlabel][time]
        kk = list(eqt.keys())[0]
        plot_equilibrium.plot_eq2D(eqt[kk],ax,bndry=True,bndryEq=eq)

        # plot TS positions
        nchannels = len(self.thomson[shotlabel]["channels"])
        for i in range(nchannels):
            rp = self.thomson[shotlabel]["r"][i]
            zp = self.thomson[shotlabel]["z"][i]
            ax.plot(rp,zp,'rx',mew=2)
        
        if (showplot):
            plt.show()
            plt.close()

        return

    def plot_ne_profile(self, shotlabel, ax=None, time=None):
        """
        Plot the density profile vs normalized psi
        """
        showplot = False
        if (ax == None):
            fig = plt.figure(figsize=(6,6))
            ax = fig.add_subplot(111)
            showplot = True

        if (time==None):
            tt = list(self.get_times(shotlabel))
            time = tt[len(tt)//2]

        # get profile
        if (self.haveprofiles):
            psi = self.thomson[shotlabel]["profiles"]["psi"][time]
            ne = self.thomson[shotlabel]["profiles"]["n_e_psi"][time]
            ne_err = self.thomson[shotlabel]["profiles"]["n_e_err_psi"][time]
        else:
            raise Exception("no profile data to plot")

        # try to get fit data if it is there
        if (self.havefits):
            fpsi = self.thomson[shotlabel]["profiles"]["psi_fit"][time]
            fne = self.thomson[shotlabel]["profiles"]["n_e_fit"][time]
            fne_err = self.thomson[shotlabel]["profiles"]["n_e_err_fit"][time]

            ax.plot(fpsi, fne, '-', color='red')
            ax.fill_between(fpsi, fne - 1.96*fne_err, fne + 1.96*fne_err,
                 color='red', alpha=0.2)

        ax.errorbar(psi,ne,yerr=ne_err,marker='o',mfc='none',mec='black',linestyle='')
        ax.set_xlabel(r'$\psi_N$')
        ax.set_ylabel(r'density [m$^{-3}$]')
        if (showplot):
            plt.show()
            plt.close()

        return

    def plot_te_profile(self, shotlabel, ax=None, time=None):
        """
        Plot the temperature profile vs normalized psi
        """
        showplot = False
        if (ax == None):
            fig = plt.figure(figsize=(6,6))
            ax = fig.add_subplot(111)
            showplot = True

        if (time==None):
            tt = list(self.get_times(shotlabel))
            time = tt[len(tt)//2]

        # get profile
        if (self.haveprofiles):
            psi = self.thomson[shotlabel]["profiles"]["psi"][time]
            te = self.thomson[shotlabel]["profiles"]["t_e_psi"][time]
            te_err = self.thomson[shotlabel]["profiles"]["t_e_err_psi"][time]
        else:
            raise Exception("no profile data to plot")

        # try to get fit data if it is there
        if (self.havefits):
            fpsi = self.thomson[shotlabel]["profiles"]["psi_fit"][time]
            fte = self.thomson[shotlabel]["profiles"]["t_e_fit"][time]
            fte_err = self.thomson[shotlabel]["profiles"]["t_e_err_fit"][time]

            ax.plot(fpsi, fte, '-', color='red')
            ax.fill_between(fpsi, fte - 1.96*fte_err, fte + 1.96*fte_err,
                 color='red', alpha=0.2)

        ax.errorbar(psi,te,yerr=te_err,marker='o',mfc='none',mec='black',linestyle='')
        ax.set_xlabel(r'$\psi_N$')
        ax.set_ylabel(r'temperature [eV]')
        if (showplot):
            plt.show()
            plt.close()

        return
    
    def plot_all(self, shotlabel, time=None, savefilename=None, noShow=False):
        """
        Plot the channel positions on top of the equilibrium on the left side,
        plot both density and temperature profiles on the right side
        """
        if (time==None):
            tt = list(self.get_times(shotlabel))
            time = tt[len(tt)//2]

        print("Plotting data for t=",time)
        fig, axd = plt.subplot_mosaic([['left', 'upper right'], ['left', 'lower right']], figsize=(12, 10), constrained_layout=True)
        plt.suptitle('EFIT Equilibrium and Thomson Scattering Data, DIII-D shot #'+str(shotlabel).split('_')[1]+', t='+str(time)+'s')
        self.plot_eq_channel(shotlabel,ax=axd["left"],time=time)
        self.plot_ne_profile(shotlabel,ax=axd["upper right"],time=time)
        self.plot_te_profile(shotlabel,ax=axd["lower right"],time=time)
        if (savefilename != None):
            if (not (savefilename.endswith('.png'))):
                print("Cannot save figure, please make sure file name includes .png extension")
                exit()
            plt.savefig(savefilename,dpi=150)
        if not noShow:
            plt.show()
        plt.close()
 
    def plot_pedestal(self, shotlabel, savefilename=None, noShow=False):
        """
        Plot time traces of the pedestal width and location
        """
        if (not self.havefits):
            raise Exception("no fit data to plot")

        t = np.empty(0)
        neped = np.empty(0)
        newid = np.empty(0)
        teped = np.empty(0)
        tewid = np.empty(0)
        for timenm in get_times(shotlabel):
            npe = self.thomson[shotlabel]["profiles"]["neped_fit"][timenm]
            nw = self.thomson[shotlabel]["profiles"]["newid_fit"][timenm]
            tp = self.thomson[shotlabel]["profiles"]["teped_fit"][timenm]
            tw = self.thomson[shotlabel]["profiles"]["tewid_fit"][timenm]
            t = np.append(t,float(timenm))
            neped = np.append(neped,float(npe))
            newid = np.append(newid,float(nw))
            teped = np.append(teped,float(tp))
            tewid = np.append(tewid,float(tw))
        
        fig = plt.figure(figsize=(12,4))
        plt.suptitle("Pedestal Evolution DIII-D shot #"+shotlabel.split("_")[1], fontsize=14)
        ax = fig.add_subplot(121)
        ax.plot(t,neped,label=r'$n_e$')
        ax.plot(t,teped,label=r'$T_e$')
        ax.set_xlabel('time [s]')
        ax.set_ylabel(r'pedestal location [$\psi_N$]')
        plt.legend()

        ax = fig.add_subplot(122)
        ax.plot(t,newid)
        ax.plot(t,tewid)
        ax.set_xlabel('time [s]')
        ax.set_ylabel(r'pedestal width [$\psi_N$]')

        plt.tight_layout()
        if (savefilename != None):
            if (not (savefilename.endswith('.png'))):
                print("Cannot save figure, please make sure file name includes .png extension")
                exit()
            plt.savefig(savefilename,dpi=150)
        if not noShow:
            plt.show()
        plt.close()

def parse_thomsonargs():
    """
    Routine for getting the arguments for thomson -- but this calls the
    parse_eqargs because we can reuse those (this is a form of inheritance)
    """
    parser = parse_eqargs()
    parser.add_argument(
        "-k",
        "--keep_equilibrium",
        help="Keep equilibrium data with the magnetics data",
        dest="keep_equilibrium",
        action="store_true",
    )
    return parser


def main():
    """
    Convert
    """
    parser = parse_thomsonargs()
    args = parser.parse_args()

    # Sanity checks
    if args is None:
        parser.print_usage()
        return

    input_file = args.fileName

    if not os.path.exists(input_file):
        print("Input file ", input_file, " must exist.")
        return
    if os.path.splitext(input_file)[1] not in [".h5"]:
        print("Input file ", input_file, " must be an h5 file.")
        return

    tc = ThomsonChannels(args)
    tc.get_from_file(input_file)
    shotlbl = list(tc._stored_data.keys())[0]
    tc.time_ave(shotlbl,window=100.)
    tc.get_ts_as_psi(shotlbl)
    tc.write_to_file(shotlbl,input_file)

    plotAll = False
    if (plotAll):
        for t in tc.get_times(shotlbl):
            tc.plot_all(shotlbl,time=t,noShow=True,savefilename="thomson_profiles_t"+str(t)+".png")
    else:
        tc.plot_all(shotlbl,savefilename="thomson_profiles.png")

    # time_get = 4.06115405


if __name__ == "__main__":
    main()
