#!/usr/bin/env python
"""
File to validate mse data to see if the conform to various EFIT-AI standards
"""
import eqdb_utils
import numpy as np


def get_valid_timeslices(hin, verbose=False):
    """
    Function for keeping only the time slices with reasonable values
    of BetaN, L_i, q95, q0. Additional filter based on flat top phase or dIp/dt
    are also included.
    ** Time slices containing MSE zeroes are also discarded here.
      Inputs:
      -------
        hin: h5py handle of file (must be opened outside of routine)
      Outputs:
      -------
        valid_slices:    1D array of indices of the time slices that pass
                          validation tests
        num_all_slices : scalar integer, number of original time slices
    """
    def running_mean(x, N):
        """ x == an array of data. N == number of samples per average """
        cumsum = np.cumsum(np.insert(x, 0, 0))
        return (cumsum[N:] - cumsum[:-N]) / float(N)

    def get_global_quant(time_slice):
        """
        Function for pulling the following items from the ODS data
        ['beta_normal', 'beta_tor', 'ip', 'li_3', 'magnetic_axis',
          'psi_axis', 'psi_boundary', 'q_95', 'q_axis', 'q_min']
        """
        R0 = time_slice["global_quantities"]["magnetic_axis"]["r"][()]
        Z0 = time_slice["global_quantities"]["magnetic_axis"]["z"][()]
        BetaN = time_slice["global_quantities"]["beta_normal"][()]
        li = time_slice["global_quantities"]["li_3"][()]
        # convert to MA
        Ip = 1.0e-6 * time_slice["global_quantities"]["ip"][()]
        q_axis = time_slice["global_quantities"]["q_axis"][()]
        q95 = time_slice["global_quantities"]["q_95"][()]
        q_min = time_slice["global_quantities"]["q_min"]["value"][()]

        return R0, Z0, BetaN, li, abs(q95), abs(q_axis), abs(q_min), Ip

    efit_type = eqdb_utils.get_efit_type(hin, verbose)

    times = hin.get("equilibrium/time")[()]
    time_slices = np.arange(times.size)

    EFITglobal_scalars = np.zeros((8, len(time_slices)))
    for i in time_slices:
        tgrp = hin.get("equilibrium/time_slice/" + str(i))
        EFITglobal_scalars[:, i] = get_global_quant(tgrp)

    # define the hard limits for filtering data
    # TODO:  Need to make this consistent with Torrin's data
    BetaNlim = 8.0
    lilimUp = 2.0
    lilimLow = 0.05
    q95lim = 15.0
    q0lim = 10.0

    # add the flat top threshold
    Ip = EFITglobal_scalars[-1, :]  # Ip is in MA here

    # Calculate dIp/dt with a smoothed Ip
    Nc = 5  # how many points to keep for the running average
    if len(times) < 5:
        print("\nSignal too short! ERROR")
        return [], len(time_slices)
    else:
        Nh = int((Nc - 1) / 2.0)
    Ismooth = running_mean(Ip, Nc)  # NOTE len(Ismooth) = len(Ip) -(Nc-1) !!
    # skip the 1st few locations to keep the array lengths consistent
    # in the filter below
    dIp_dt = np.zeros(len(Ip))
    dIp_dt[Nh:-Nh] = np.gradient(Ismooth, times[Nh:-Nh])
    dIp_dt[-Nh:] = dIp_dt[-Nh - Nh: -Nh]  # repeat last bit

    # TODO:  This doesn't have all of the checks that Torrin's does
    # The following filters return a list of True's and False's, where the
    # True's correspond to the `good' time slices
    goodSlices = (
        (EFITglobal_scalars[2, :] <= BetaNlim)
        & (EFITglobal_scalars[3, :] <= lilimUp)
        & (EFITglobal_scalars[3, :] > lilimLow)
        & (EFITglobal_scalars[4, :] <= q95lim)
        & (EFITglobal_scalars[5, :] <= q0lim)
    )
    # For machine learning, Cihan filters on dIpdt
    #    & (Ip >= 0.1)
    #    & (dIp_dt > -2.0)
    # IpFT = 0.95 * Ip.max()
    #           & (Ip >= IpFT) # turn ON for FLAT-TOP
    # TODO:  Need to handle dIpdt better

    Nt = sum(goodSlices)  # total number of good slices

    # the following code is required to flag bad EFIT02 & kin MSE data
    if efit_type == "02" or efit_type == "kin":
        num_MSE = 11  # HARD-CODED number of MSE chords we are using
        # initial vector of indices
        nonzeroMSE = np.linspace(0, Nt - 1, Nt, dtype=int)
        SumSigmaMSE = np.zeros(Nt)
        for it, i in enumerate(time_slices[goodSlices]):
            insgrp = hin.get(f"equilibrium/code/parameters/time_slice/{i}/ins")
            sigmaMSE = insgrp["sgamma"][()][:num_MSE]
            SumSigmaMSE[it] = np.sum(sigmaMSE)

        nonzeroMSE = np.where(SumSigmaMSE > 1.0e-5)[0]
        finalSlices = time_slices[goodSlices][nonzeroMSE]
        if not len(nonzeroMSE):
            print("MSE alert! NO good slices found in this OMAS file")
    else:  # for EFIT01, simply pass the good time indices to nonzeroMSE array
        finalSlices = time_slices[goodSlices]

    return finalSlices, len(time_slices)
