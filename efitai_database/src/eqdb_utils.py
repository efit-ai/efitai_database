"""
Various utilities for working with the efitai database
"""
import h5py
import numpy as np


def write_dict(d, h5root):
    """
    Given a nested dictionary, write an h5 file with a tree-structure matching
     the nesting (using generators)
    """
    for key, value in d.items():
        # print(key)
        if isinstance(value, dict):
            if key not in h5root:
                h5sub = h5root.create_group(key)
            else:
                h5sub = h5root.get(key)
            yield from write_dict(value, h5sub)
        else:
            yield key, value, h5root


def getdbvals(node, exclude=[], verbose=False):
    """Traverse an h5py node and stuff results into nested dictionary"""
    d = {}
    parent = str(node.name)

    def get_items(name, item, d=d, parent=parent):
        # Newer versions of visitpath forced a change
        # in this method
        itemlst = name.split("/")

        # if intersection is non-zero
        if list(set(exclude) & set(itemlst)):
            return
        p = d
        if isinstance(item, h5py.Dataset):
            for x in itemlst[:-1]:
                p = p.setdefault(x, {})
            p = p.setdefault(itemlst[-1], item[()])
            if verbose:
                print("      ", name)
        elif isinstance(item, h5py.Group):
            if verbose:
                print("      ", name)
        else:
            print("Problem with " + name, type(item))

    node.visititems(get_items)
    return d


# TODO: Figure out how to map this onto other types, whether to store it, etc.
def get_efit_type(h5root, verbose=False):
    """ look for the 'ins' key and if it's there, it's EFIT02
        input: h5py root handle of the file
    """
    eqgrpstr = "equilibrium/code/parameters/time_slice/0/"
    eqgrpkeys = h5root.get(eqgrpstr).keys()

    if "ins" in eqgrpkeys:
        efit_type = "02"
        if verbose:
            print("This OMAS .h5 file was generated with EFIT02")
    # CASE for Kin EFITs to be added
    # elif [param] in somekeys
    else:
        efit_type = "01"
        if verbose:
            print("This OMAS .h5 file was generated with EFIT01")

    return efit_type


def list_times_in_file(input_file=None, h5in=None, sprint=True):
    """
    Print the time slices and their associated times that there are equilibrium
    """
    cleanup = False
    if input_file:
        h5in = h5py.File(input_file, mode="r")
        cleanup = True
    elif not h5in:
        print("Must specify either an input_file or h5 root node")
        return

    # Can contain multiple equilibria
    eqgrp = h5in.get("equilibrium")

    # Time slices
    time = eqgrp.get("time")[()]
    time_slices = np.arange(time.size)

    if cleanup:
        h5in.close()

    if sprint:
        # print("Slice", "\t", "time [s]")
        for ts in time_slices:
            print(ts, "\t", time[ts])
        return
    else:
        return time
