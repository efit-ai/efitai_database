
Extracting Data
================

Extracting Equilibrium
----------------------

As in prior documentation, we assume that you are running the scripts from
`efitai_database/src`.  It is straightforward to generalize to the situation
where it is installed into your path.

Show the time slices and times that are listed in a file::

    extract_equilibrium.py --list ../data/diii-d/165908_pankin_165908_test.h5

Extract the first time slice from the file::

    extract_equilibrium.py -s 0 ../data/diii-d/165908_pankin_165908_test.h5

This doesn't actually do anything -- the data is loaded into memory and then
nothing is put into the file.  Let's cache that data and show what's going on
while we are running (verbose setting)::

    extract_equilibrium.py -v -s 0 -o eqs.h5 ../data/diii-d/165908_pankin_165908_test.h5

Let's see how the caching stores the data::

     ➜ h5ls -r eqs.h5  | head -5
     /                                              Group
     /DIII-D_165908                                 Group
     /DIII-D_165908/3.1                             Group
     /DIII-D_165908/3.1/eq2000380601                Group
     /DIII-D_165908/3.1/eq2000380601/boundary       Group


The `3.1` here refers the time of 3.1 seconds of shot `165908` on the `DIII-D`
experimental machine.  The `eq2000380601` is a label for that specific
equilibrium because any given time slice can have multiple equilibria (magnetic
EFIT, kinetic EFIT, ...).

Plotting Equilibrium
--------------------

Let's plot the equilibria in a file (this uses the extraction code above), but
given the large number of slices, only plot slice 10::

   plot_equilibrium.py -s 10 179565.h5

This should plot the following image:

.. image:: figures/eq010.png
  :alt: Equilibrium slice 10 figure

To create that image, you can add the `-S` flag.   You can plot multiple time slices:

   plot_equilibrium.py -s 10,11,12 179565.h5

There is a limit of 20 figures that one can plot.   To see all of the figures, 
it's best to save them to a file:

   plot_equilibrium.py -S 179565.h5

This will give a message about how to make an animated gif using the popular
command-line ImageMagic utilities because that is often useful.

To show the one-dimension plots:

   plot_equilibrium.py -s 10 --plot_1d 179565.h5


Extracting Magnetics
----------------------

This is still being developed.



Extracting Thomson
----------------------

This is still being developed.



Extracting the EFIT K-file
--------------------------

This is still being developed.


