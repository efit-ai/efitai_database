.. _quickstart:

Quickstart
==========

`efitai_database` is best thought of as a collection of tools.  Here, we
provide the quick starts for each tool area.  

First, let's assume you have either the full EFIT-AI database, a subset of it, 
or a generic IMAS file.   Here we will just give an example of the simplest 
script to ensure that things are installed correctly, and things work.  This 
serves as a basic template for all that follows.

The script we will examine is `list_equlibria.py`.   Assuming that it is in
your path, you can see the command-line arguments, by using the `-h` option::

   ➜ list_equilibria.py -h
  usage: list_equilibria.py [-h] [-v] [-c] files [files ...]

  List the equilibria times and count in the files given

  positional arguments:
    files          EFIT-AI Database files

  options:
    -h, --help     show this help message and exit
    -v, --verbose  Verbose output
    -c, --count    Instead of listing, just count the
                   equilibria


All command-line scripts will take the `-h` argument to enable one to 
rapidly see how to use it without reading the documentation.   The 
`--verbose` option is useful if things do not work as expected:  more verbosity
hopefully will explain what happens.

Let's run it on a sample IMAS file (locate in efitai_database/data/diii-d)::

  ➜ list_equilibria.py 179565.h5
  179565.h5
           0       0.1 s
           1       0.12 s
           2       0.14 s
           ...
           89      1.96 s
           90      1.98 s
  Total number of equilibria: 91

This shows all of the times in the file numbered in the same way that IMAS 
does.   If all we want to do is count the equilibria in the file::

  ➜ list_equilibria.py -c 179565.h5
  179565.h5        13
  Total number of equilibria: 13

If you want to count the number of equilibria in an entire directory::

  ➜ list_equilibria.py -c *.h5

We leave that as an exercise for the reader.


