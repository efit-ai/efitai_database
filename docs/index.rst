
efitai_database
================

efitai_database

Welcome to efitai_database's documentation.  efitai_database is part of the 
`EFIT-AI`_ project.   

The database is a collection of IMAS-formatted HDF5 files in a particular
directory structure along with metadata and accessiblity format.  The utilities
here are meant for the needs of the EFIT-AI database, but many of the utilities
can be used independently for an IMAS-formatted file.

The collection of python scripts in the repository are meant to be used either 
as 1) stand-alone applications that can be invoked from the command-line, 
2) used as a library of useful functions with a fixed API, and 3) used from 
within a Jupyter notebook easily.   The documentation will focus on the 
command-line interface the most to give a sense of the capabilities.  The
API is documented in the developer's section.   Example notebooks are also
explained.

The tools here were critical for making the database FAIR-compliant, and 
then to use the database.   We group the scripts according to their 
function, and they are described next.


User documentation
~~~~~~~~~~~~~~~~~~
.. toctree::
   :maxdepth: 2
   
   install
   quickstart
   validate
   extract
   export
   license

Developer documentation
~~~~~~~~~~~~~~~~~~~~~~~~
.. toctree::
   :maxdepth: 2
   
   developer
   api
   
efitai_database areas
~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2
   
   database
   workflow


.. _EFIT-AI: https://fusion.gat.com/conference/event/110

.. include:: contents.rst.inc
