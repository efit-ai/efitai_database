.. _workflow:

Workflow for Extending the Database
===================================

The database is composed of complete, self-descriptive, and accurate IMAS equilibria.  Construction requires attention to these details, but methods have been developed in the newest version of EFIT and OMFIT to make this easier.  Careful checking must be done if cases are added by hand.  To automate the process and make it as quick as possible, a workflow has been developed for producing new datasets on the NERSC system.  This uses their taskfarmer process as described in further detail below.

Taskfarmer
==========

.. include:: ../efitai_database/src/workflow/taskfarmer.rst

